/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.prism.cratesreloaded;

import java.util.ArrayList;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class ItemFilter {

    private static ArrayList<String> items = new ArrayList<>();

    public ItemFilter() {
        items.add("&c[&6Mystery &eKey&c] &7Roulette-Crate Key");
    }

    public static boolean inspect(ItemStack item) {
        if (item.getType() == Material.LEVER) {
            if (item.hasItemMeta()) {
                if (item.getItemMeta().hasDisplayName()) {
                    for (String i : items) {
                        if (item.getItemMeta().getDisplayName().equalsIgnoreCase(i)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

}
